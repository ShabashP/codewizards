import numpy as np

pi_half = 0.5*np.pi


def distance_to_line(x0, y0, x1, y1, x, y):
    dy, dx = y0 - y1, x1 - x0
    return np.abs(dy*x + dx*y + x0*y1 - x1*y0)/np.sqrt(dx**2 + dy**2)


# noinspection PyTypeChecker
def angle_to_point(x, y, angle, x1, y1):
    absolute_angle_to = np.arctan2(y1 - y, x1 - x)
    relative_angle_to = absolute_angle_to - angle

    while np.any(relative_angle_to > np.pi):
        relative_angle_to[np.argwhere(relative_angle_to > np.pi)] -= 2.0 * np.pi

    while np.any(relative_angle_to < -np.pi):
        relative_angle_to[np.argwhere(relative_angle_to < -np.pi)] += 2.0 * np.pi

    return relative_angle_to


def angle_to_point_scalar(x, y, angle, x1, y1):
    absolute_angle_to = np.arctan2(y1 - y, x1 - x)
    relative_angle_to = absolute_angle_to - angle

    while relative_angle_to > np.pi:
        relative_angle_to -= 2.0 * np.pi

    while relative_angle_to < -np.pi:
        relative_angle_to += 2.0 * np.pi

    return relative_angle_to


def linear_interpolation(x, x1, x2, y1, y2):
    return (x - x1)*(y2 - y1)/(x2 - x1) + y1
