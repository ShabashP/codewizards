# coding=utf-8
from model.ActionType import ActionType
from model.Game import Game
from model.Move import Move
from model.Wizard import Wizard
from model.World import World
from model.Faction import Faction
from model.Tree import Tree
from model.Minion import Minion
from model.MinionType import MinionType
from model.Building import Building
from model.BuildingType import BuildingType
from model.LaneType import LaneType
from model.SkillType import SkillType
from model.StatusType import StatusType

import warnings
import numpy as np
from time import time
from collections import deque
from scipy.spatial.distance import cdist, pdist, squareform

from state import WorldState
from predictor import Predictor, DEPTH
from router import Router, BASE_ENEMIES, BASE_ALLIES
from potential import *
from utils import angle_to_point, distance_to_line


try:
    from debug_client import DebugClient
except:
    debug = None
else:
    debug = None # DebugClient()


warnings.filterwarnings("ignore", category=RuntimeWarning)
warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)

SCAN_RANGE_FACTOR = 1.75


def is_neutral_aggressive(minion):
    """
    :type minion: Minion
    :rtype: bool
    """
    return minion.speed_x != 0.0 or minion.speed_y != 0.0 \
            or 0 != minion.remaining_action_cooldown_ticks \
            or minion.life != minion.max_life


FROST = [
    SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_1,
    SkillType.MAGICAL_DAMAGE_BONUS_AURA_1,
    SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_2,
    SkillType.MAGICAL_DAMAGE_BONUS_AURA_2,
    SkillType.FROST_BOLT,
]
FIREBALL = [
    SkillType.STAFF_DAMAGE_BONUS_PASSIVE_1,
    SkillType.STAFF_DAMAGE_BONUS_AURA_1,
    SkillType.STAFF_DAMAGE_BONUS_PASSIVE_2,
    SkillType.STAFF_DAMAGE_BONUS_AURA_2,
    SkillType.FIREBALL,
]
RANGE = [
    SkillType.RANGE_BONUS_PASSIVE_1,
    SkillType.RANGE_BONUS_AURA_1,
    SkillType.RANGE_BONUS_PASSIVE_2,
    SkillType.RANGE_BONUS_AURA_2,
    SkillType.ADVANCED_MAGIC_MISSILE,
]
SHIELD = [
    SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_1,
    SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_1,
    SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_2,
    SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_2,
    SkillType.SHIELD
]

DEFAULT_SKILLS = FIREBALL + RANGE + SHIELD + FROST

SKILLS = {
    0: FIREBALL + SHIELD + RANGE + FROST,
    4: FIREBALL + SHIELD + RANGE + FROST,
    1: FIREBALL + SHIELD + RANGE + FROST,
    2: FROST + SHIELD + RANGE + FIREBALL,
    3: SHIELD + FROST + RANGE + FIREBALL,
}


class MyStrategy:

    depth = float(DEPTH)

    def __init__(self):
        self.t0 = 0.0
        self.tick = -1
        self.predictor = None
        self.router = None
        self.enemy_faction = None
        self.level = 0
        self.potential_enemy_orc = None
        self.potential_enemy_blowdart = None
        self.potential_enemy_tower = None
        self.potential_neutral_minion = None
        self.potential_allies_wizard = None
        self.potential_allies_minion = None
        self.potential_allies_tower = None
        self.potential_allies_base = None
        self.potential_empty = None
        self.potential_minion_staff = None

        self.damage_per_tick_orc = 0.0
        self.damage_per_tick_blowdart = 0.0
        self.damage_per_tick_tower = 0.0
        self.damage_per_tick_base = 0.0

        self.skills_to_learn = []
        self.attack_sector = 0.0

        self.towers = {}

    def plot(self, axis, game, n=10, x0=None, y0=None, x1=None, y1=None):
        """
        :type axis: axis
        :type game: Game
        :type n: int
        :type x0: float
        :type y0: float
        :type x1: float
        :type y1: float
        """
        for x in np.linspace(x0 or 0, x1 or game.map_size, num=n):
            for y in np.linspace(y0 or 0, y1 or game.map_size, num=n):
                axis.text(x, y, "%3.4f" % self.router.get_potential(x, y), fontsize=8.0)

    # noinspection PyTypeChecker
    def move(self, me, world, game, move):
        """
        :type me: Wizard
        :type world: World
        :type game: Game
        :type move: Move
        """

        t0 = time()

        if world.tick_index == 0:
            self.t0 = time()
            self.enemy_faction = Faction.RENEGADES if me.faction == Faction.ACADEMY else Faction.ACADEMY

            self.potential_enemy_orc = EnemyOrcPotential(game)
            self.potential_enemy_blowdart = EnemyBlowdartPotential(game)

            self.potential_allies_wizard = SkinPotential(
                2.0*game.wizard_radius, 1.0
            )

            self.potential_allies_minion = SkinPotential(
                game.minion_radius + game.wizard_radius,
                1.0
            )

            self.potential_allies_tower = SkinPotential(
                game.guardian_tower_radius + game.wizard_radius,
                2.0,
                k2=0.05
            )
            self.potential_allies_base = SkinPotential(
                game.faction_base_radius + game.wizard_radius,
                2.0,
                k2=0.05
            )
            self.potential_enemy_tower = EnemyTowerPotential(game)  # TowerPotential(game)

            self.potential_neutral_minion = SkinPotential(
                game.minion_radius + game.wizard_radius,
                2.0, 0.05, k2=0.001
            )
            self.potential_minion_staff = EnemyMinionStaffPotential(game)

            self.potential_empty = EmptyPotential()

            self.damage_per_tick_orc = float(game.orc_woodcutter_damage) / float(game.orc_woodcutter_action_cooldown_ticks)
            self.damage_per_tick_blowdart = float(game.dart_direct_damage) / float(game.fetish_blowdart_action_cooldown_ticks)
            self.damage_per_tick_tower = 2.5 * float(game.guardian_tower_damage) / float(game.guardian_tower_cooldown_ticks)
            self.damage_per_tick_base = 2.0 * float(game.faction_base_damage) / float(game.faction_base_cooldown_ticks)

            if game.raw_messages_enabled:
                for i, w in enumerate(sorted(world.wizards, key=lambda ww: ww.id)):
                    if not w.me:
                        continue
                    self.skills_to_learn = deque(SKILLS.get(i, DEFAULT_SKILLS))
                    if i == 0:
                        self.router = Router(game, me, LaneType.TOP)
                    elif i == 4:
                        self.router = Router(game, me, LaneType.BOTTOM)
                    else:
                        self.router = Router(game, me, LaneType.MIDDLE)
            else:
                # if me.messages and me.messages[0].lane is not None and not me.master:
                #     self.router = Router(game, me, me.messages[0].lane)
                # else:
                #     self.router = Router(game, me)
                self.router = Router(game, me, LaneType.TOP)
                self.skills_to_learn = deque(DEFAULT_SKILLS)

            assert self.router is not None

            self.predictor = Predictor(game)
            self.attack_sector = 0.5 * game.staff_sector

            bx, by = BASE_ALLIES
            ex, ey = BASE_ENEMIES
            for building in world.buildings:
                if building.type == BuildingType.GUARDIAN_TOWER:
                    self.towers[(np.round(ex - (building.x - bx), 2), np.round(ey - (building.y - by), 2))] = self.potential_enemy_tower
            return

        towers = self.towers.copy()
        if me.level != self.level and len(self.skills_to_learn) > 0:
            self.level = int(me.level)
            move.skill_to_learn = self.skills_to_learn.popleft()
        health_factor = float(me.life) / float(me.max_life)
        mana_factor = float(me.mana) / float(me.max_mana)
        can_attack = me.remaining_action_cooldown_ticks == 0
        can_use_staff = can_attack and me.remaining_cooldown_ticks_by_action[ActionType.STAFF] == 0
        can_use_magic_missile = can_attack and me.remaining_cooldown_ticks_by_action[ActionType.MAGIC_MISSILE] == 0 and me.mana >= game.magic_missile_manacost
        can_use_frost_bolt = game.skills_enabled and can_attack and me.remaining_cooldown_ticks_by_action[ActionType.FROST_BOLT] == 0 and SkillType.FROST_BOLT in me.skills and me.mana >= game.frost_bolt_manacost
        can_use_fireball = game.skills_enabled and can_attack and me.remaining_cooldown_ticks_by_action[ActionType.FIREBALL] == 0 and SkillType.FIREBALL in me.skills and me.mana >= game.fireball_manacost
        can_use_shield = game.skills_enabled and can_attack and me.remaining_cooldown_ticks_by_action[ActionType.SHIELD] == 0 and SkillType.SHIELD in me.skills and me.mana >= game.shield_manacost
        player_damage_per_tick = float(game.magic_missile_direct_damage) / float(game.magic_missile_cooldown_ticks)
        if StatusType.EMPOWERED in me.statuses:
            player_damage_per_tick *= game.empowered_damage_factor

        scan_range, i = SCAN_RANGE_FACTOR * me.cast_range, 0

        units, potentials = [], []
        positions, radius, units_types = [], [], []
        initial_distances = []
        unit_angles, speed = [], []
        scan_units, sim_units = [], []
        allies, enemies, neutrals = [], [], []
        u_damage_per_tick = []
        nearby_enemies_wizards, nearby_allies_wizards = [], []
        any_nearby_enemies_wizards_dangerous = False
        nearby_enemies_base = None
        enemy_towers = []
        # -------------------------------------------------------------------------------------------------------------
        # MINIONS
        # -------------------------------------------------------------------------------------------------------------
        for minion in world.minions:
            units.append(minion)
            units_types.append(Minion)
            positions.append((minion.x, minion.y))
            unit_angles.append(minion.angle)
            speed.append((minion.speed_x, minion.speed_y))
            radius.append(minion.radius)
            d = me.get_distance_to_unit(minion)
            initial_distances.append(d)

            aggressive = minion.faction == self.enemy_faction \
                         or (minion.faction == Faction.NEUTRAL and is_neutral_aggressive(minion))

            if minion.type == MinionType.ORC_WOODCUTTER:
                u_damage_per_tick.append(self.damage_per_tick_orc)
                if minion.faction == me.faction:
                    potentials.append(self.potential_allies_minion)
                    allies.append(i)
                    if d <= self.predictor.max_offset:
                        scan_units.append(i)
                        sim_units.append(i)
                elif aggressive:
                    potentials.append(self.potential_enemy_orc)
                    enemies.append(i)
                    if d <= scan_range:
                        scan_units.append(i)
                    if d <= self.predictor.max_offset:
                        sim_units.append(i)
                else:
                    potentials.append(self.potential_neutral_minion)
                    neutrals.append(i)
                    if d <= scan_range:
                        scan_units.append(i)
            else:
                u_damage_per_tick.append(self.damage_per_tick_blowdart)
                if minion.faction == me.faction:
                    potentials.append(self.potential_allies_minion)
                    allies.append(i)
                    if d <= self.predictor.max_offset:
                        scan_units.append(i)
                        sim_units.append(i)
                elif aggressive:
                    potentials.append(self.potential_enemy_blowdart)
                    enemies.append(i)
                    if d <= scan_range:
                        scan_units.append(i)
                    if d <= self.predictor.max_offset:
                        sim_units.append(i)
                else:
                    potentials.append(self.potential_neutral_minion)
                    neutrals.append(i)
                    if d <= scan_range:
                        scan_units.append(i)
            i += 1

        # -------------------------------------------------------------------------------------------------------------
        # WIZARDS
        # -------------------------------------------------------------------------------------------------------------
        for wizard in world.wizards:

            if wizard.me:
                continue
            units_types.append(Wizard)
            units.append(wizard)
            positions.append((wizard.x, wizard.y))
            radius.append(wizard.radius)
            speed.append((wizard.speed_x, wizard.speed_y))
            unit_angles.append(wizard.angle)

            d = me.get_distance_to_unit(wizard)
            initial_distances.append(d)

            if d <= scan_range:
                scan_units.append(i)

            # FIXME зависимость от умений
            u_damage_per_tick.append(float(game.magic_missile_direct_damage) / float(game.magic_missile_cooldown_ticks))
            if StatusType.EMPOWERED in wizard.statuses:
                player_damage_per_tick *= game.empowered_damage_factor

            if wizard.faction == me.faction:
                potentials.append(self.potential_allies_wizard)
                allies.append(i)
                if d <= me.cast_range:
                    nearby_allies_wizards.append(i)
                if d <= self.predictor.max_offset:
                    sim_units.append(i)
            else:
                if d <= me.cast_range + game.wizard_radius:
                    nearby_enemies_wizards.append(i)
                if d <= self.predictor.max_offset:
                    sim_units.append(i)
                potentials.append(EnemyWizardPotentialOff(wizard, game, health_factor, me))
                if d <= wizard.cast_range + me.radius and abs(wizard.get_angle_to_unit(me)) <= self.attack_sector:
                    any_nearby_enemies_wizards_dangerous = True
                enemies.append(i)
            i += 1

        # -------------------------------------------------------------------------------------------------------------
        # BUILDINGS
        # -------------------------------------------------------------------------------------------------------------
        visible_enemy_towers = []
        for building in world.buildings:  # FIXME начальное положение вражеских башен

            units.append(building)
            units_types.append(Building)
            positions.append((building.x, building.y))
            speed.append((0.0, 0.0))
            unit_angles.append(np.nan)

            d = me.get_distance_to_unit(building)
            radius.append(building.radius)
            initial_distances.append(d)

            if building.faction == me.faction:
                if building.type == BuildingType.GUARDIAN_TOWER:
                    u_damage_per_tick.append(self.damage_per_tick_tower)
                    potentials.append(self.potential_allies_tower)
                    if d <= scan_range:
                        scan_units.append(i)
                else:
                    u_damage_per_tick.append(self.damage_per_tick_base)
                    potentials.append(self.potential_allies_base)
                    if d <= scan_range:
                        scan_units.append(i)
                allies.append(i)
            else:
                if building.type == BuildingType.GUARDIAN_TOWER:
                    tx, ty = np.round(building.x, 2), np.round(building.y, 2)
                    if (tx, ty) not in self.towers:
                        print "tower (%s, %s) undefined" % (building.x, building.y)
                        print self.towers
                    else:
                        towers.pop((tx, ty))
                    visible_enemy_towers.append((tx, ty))
                    u_damage_per_tick.append(self.damage_per_tick_tower)
                    potentials.append(self.potential_enemy_tower)
                    if d <= scan_range:
                        scan_units.append(i)
                        enemy_towers.append(i)
                else:
                    u_damage_per_tick.append(self.damage_per_tick_base)
                    if (float(building.life) / float(building.max_life)) > 0.1 and len([e_i for e_i in allies if building.get_distance_to_unit(units[e_i]) < 450.0]) < 2:
                        print world.tick_index, 'EnemiesBasePotentialOff'
                        potentials.append(EnemiesBasePotentialOff(game))
                    else:
                        potentials.append(EnemiesBasePotential(game))
                    if d <= scan_range:
                        scan_units.append(i)
                    if d <= me.cast_range + game.faction_base_radius:
                        nearby_enemies_base = i
                enemies.append(i)
            i += 1

        if allies:
            remove_dummy_towers = []
            for dtx, dty in towers.keys():
                for jj in allies:
                    d_t_d = units[jj].get_distance_to(dtx, dty)
                    if units_types[jj] == Minion and d_t_d < game.minion_vision_range and (dtx, dty) not in visible_enemy_towers:
                        remove_dummy_towers.append((dtx, dty))
                        break
                    elif units_types[jj] == Wizard and d_t_d < game.wizard_vision_range and (dtx, dty) not in visible_enemy_towers:
                        remove_dummy_towers.append((dtx, dty))
                        break
            for rt in remove_dummy_towers:
                print world.tick_index, 'Remove dummy tower (%s, %s)' % (rt[0], rt[1]), visible_enemy_towers
                towers.pop(rt)
                self.towers.pop(rt)

        if len(towers) > 0:
            dummy_towers = list(towers.keys())
            dummy_towers_distances = cdist(
                np.array([[me.x, me.y]]),
                np.array(dummy_towers, dtype=np.float)
            ).ravel()

            # noinspection PyTypeChecker
            for dummy_tower_index in np.argwhere(dummy_towers_distances <= scan_range):
                scan_units.append(i)
                potentials.append(towers[dummy_towers[dummy_tower_index]])
                units_types.append(Building)
                positions.append(dummy_towers[dummy_tower_index])
                initial_distances.append(dummy_towers_distances[dummy_tower_index])
                speed.append((0.0, 0.0))
                radius.append(game.guardian_tower_radius)
                unit_angles.append(np.nan)
                u_damage_per_tick.append(self.damage_per_tick_tower)
                units.append(
                    Building(
                        '',
                        dummy_towers[dummy_tower_index][0],
                        dummy_towers[dummy_tower_index][1],
                        speed_x=0.0, speed_y=0.0, angle=0.0,
                        faction=self.enemy_faction,
                        radius=game.guardian_tower_radius,
                        life=game.guardian_tower_life,
                        max_life=game.guardian_tower_life,
                        statuses=[],
                        type=BuildingType.GUARDIAN_TOWER,
                        vision_range=game.guardian_tower_vision_range,
                        attack_range=game.guardian_tower_attack_range,
                        damage=game.guardian_tower_damage,
                        cooldown_ticks=game.guardian_tower_cooldown_ticks,
                        remaining_action_cooldown_ticks=0
                    )
                )
                i += 1

        # -------------------------------------------------------------------------------------------------------------
        # PROJECTILES
        # -------------------------------------------------------------------------------------------------------------
        # for projectile in world.projectiles:
        #     units.append(projectile)
        #     units_types.append(Projectile)
        #     radius.append(projectile.radius)
        #     speed.append((projectile.speed_x, projectile.speed_y))
        #     positions.append((projectile.x, projectile.y))
        #     unit_angles.append(projectile.angle)
        #     d = me.get_distance_to_unit(projectile)
        #     initial_distances.append(d)
        #     if projectile.faction == me.faction:
        #         c.append(0.0)
        #         u_damage_per_tick.append(0.0)
        #         potentials.append(EmptyPotential())
        #     else:
        #         if d <= scan_range:
        #             scan_units.append(i)
        #             sim_units.append(i)
        #         potentials.append(ProjectilePotential(projectile, game))
        #         if projectile.type == ProjectileType.DART:
        #             c.append(r_weight*game.dart_direct_damage)
        #             u_damage_per_tick.append(3.0*game.dart_direct_damage)
        #         elif projectile.type == ProjectileType.MAGIC_MISSILE:
        #             c.append(r_weight*game.magic_missile_direct_damage)
        #             u_damage_per_tick.append(game.magic_missile_direct_damage)
        #         elif projectile.type == ProjectileType.FROST_BOLT:
        #             c.append(r_weight*game.frost_bolt_direct_damage)
        #             u_damage_per_tick.append(game.frost_bolt_direct_damage)
        #         elif projectile.type == ProjectileType.FIREBALL:  # FIXME fireball
        #             c.append(r_weight*game.fireball_explosion_max_damage)
        #             u_damage_per_tick.append(game.fireball_explosion_max_damage)
        #         else:
        #             print "Unknown projectile", projectile
        #             c.append(0.0)
        #     i += 1

        # -------------------------------------------------------------------------------------------------------------
        # TREES
        # -------------------------------------------------------------------------------------------------------------
        trees_start_index = int(len(units))
        for tree in world.trees:  # FIXME начальное положение вражеских башен

            units.append(tree)
            units_types.append(Tree)
            radius.append(tree.radius)
            d = me.get_distance_to_unit(tree)
            positions.append((tree.x, tree.y))
            speed.append((0.0, 0.0))
            unit_angles.append(np.nan)
            initial_distances.append(d)
            u_damage_per_tick.append(0.0)
            if d <= 150.0:
                scan_units.append(i)
            potentials.append(
                TreePotential(tree, game, self.router.get_potential(tree.x, tree.y))
            )
            i += 1

        # print len(c), len(units), len(potentials), len(initial_distances), len(units_types), len(speed), len(unit_angles), len(u_damage_per_tick)
        # assert len(c) == len(units) == len(potentials) == len(initial_distances) == len(units_types) == len(speed) == len(unit_angles) == len(u_damage_per_tick)
        # assert all([s_u < len(units) for s_u in scan_units])
        # assert all([s_u < len(units) for s_u in sim_units])

        positions = np.array(positions, copy=False)
        p_distances = np.array(initial_distances, copy=False)
        p_angles = angle_to_point(me.x, me.y, me.angle, positions[:, 0], positions[:, 1])

        state = WorldState(
            tick=world.tick_index,
            player=me,
            ax=np.cos(me.angle),
            ay=np.sin(me.angle),
            units=np.array(units, copy=False),
            units_types=np.array(units_types, copy=False),
            positions=positions,
            radius=np.array(radius, copy=False),
            potentials=np.array(potentials, copy=False),
            p_distances=p_distances,
            p_angles=p_angles,
            scan_range=scan_range,
            scan_units=np.array(scan_units, copy=False),
            c_scan_units=len(scan_units),
            sim_units=np.array(sim_units, copy=False),
            c_sim_units=len(sim_units),
            allies=np.array(allies, copy=False),
            enemies=np.array(enemies, copy=False),
            neutrals=np.array(neutrals, copy=False),
            speed=np.array(speed, copy=False),
            angles=np.array(unit_angles, copy=False),
            u_damage_per_tick=np.array(u_damage_per_tick, copy=False),
            player_damage_per_tick=player_damage_per_tick
        )

        nearest_enemies = [u_i for u_i in state.enemies if initial_distances[u_i] <= 400.0 and state.units_types[u_i] == Minion]
        nearest_enemy = state.nearby_enemies[p_distances[state.nearby_enemies].argmin()] if state.nearby_enemies else None

        nearest_enemy_dangerous = nearest_enemy is not None and state.units_types[nearest_enemy] == Minion and (
            (
                state.units[nearest_enemy].type == MinionType.FETISH_BLOWDART
                and p_distances[nearest_enemy] <= game.fetish_blowdart_attack_range + 1.25*game.wizard_radius + game.dart_radius
                and abs(state.units[nearest_enemy].get_angle_to_unit(me)) <= 0.5 * game.fetish_blowdart_attack_sector
            ) or (
                state.units[nearest_enemy].type == MinionType.ORC_WOODCUTTER
                and p_distances[nearest_enemy] <= game.orc_woodcutter_attack_range + game.wizard_radius
                and abs(state.units[nearest_enemy].get_angle_to_unit(me)) <= 0.5 * game.orc_woodcutter_attack_sector
            )
        )

        # noinspection PyTypeChecker
        if not any_nearby_enemies_wizards_dangerous and 0 < len(nearest_enemies) <= 2 and health_factor > 0.5:

            if state.p_distances[nearest_enemies].min() < 300.0:
                state.potentials[nearest_enemies] = self.potential_minion_staff
            else:
                state.potentials[nearest_enemies] = self.potential_empty

        for nearby_tower in enemy_towers:
            # noinspection PyTypeChecker
            tower_allies_case = np.any(
                cdist(
                    state.positions[nearby_tower].reshape(1, 2), state.positions[allies]
                ).ravel() <= game.guardian_tower_attack_range
            ) and not (nearest_enemy_dangerous or any_nearby_enemies_wizards_dangerous) if allies else False
            if float(state.units[nearby_tower].life) / float(state.units[nearby_tower].max_life) < 0.15 or me.life > 1.25*game.guardian_tower_damage:

                if tower_allies_case and state.c_nearby_enemies <= 1:
                    state.potentials[nearby_tower] = EnemyTowerPotentialStaff(game)
                elif tower_allies_case:
                    state.potentials[nearby_tower] = EnemyTowerPotentialIn(game)

        if nearest_enemy_dangerous or any_nearby_enemies_wizards_dangerous:
            if nearby_enemies_base is not None and not isinstance(state.potentials[nearby_enemies_base], EnemiesBasePotentialOff):
                state.potentials[nearby_enemies_base] = self.potential_empty
        else:
            for n_e_w in nearby_enemies_wizards:
                if float(state.units[n_e_w].life) / float(state.units[n_e_w].max_life) < 0.3:
                    state.potentials[n_e_w] = EnemyWizardPotential(
                        state.units[n_e_w], game, health_factor, me
                    )

        r_updated = self.router(state, game, self.predictor, world, health_factor)
        controls, grid, predicated, k, predicated_distances = self.predictor(
            state,
            game,
            self.router
        )
        move.speed, move.strafe_speed = controls

        move.turn = me.get_angle_to(grid[0, -1], grid[1, -1])
        move.action = ActionType.NONE
        if ((nearest_enemy is None) or (nearest_enemy is not None and p_distances[nearest_enemy] > 150.0)) and not (can_attack or nearest_enemy_dangerous):
            return

        trees_indices = range(trees_start_index, p_distances.size)
        nearby_trees = [
            ti for ti in trees_indices
            if state.p_distances[ti] <= me.cast_range and abs(state.p_angles[ti]) <= self.attack_sector
        ]

        fireball_target = None
        if can_use_fireball and state.c_nearby_enemies > 2:
            n_e_pairwise_distances = squareform(
                pdist(state.positions[state.nearby_enemies])
            )

            for n_e_i, n_e_dist in enumerate(n_e_pairwise_distances):
                if np.count_nonzero(n_e_dist < game.fireball_explosion_max_damage_range) > 2:
                    fireball_target = state.nearby_enemies[n_e_i]
                    break

            if fireball_target is None:
                for n_e_i, n_e_dist in enumerate(n_e_pairwise_distances):
                    if np.count_nonzero(n_e_dist < game.fireball_explosion_min_damage_range) > 2:
                        fireball_target = state.nearby_enemies[n_e_i]
                        break

        tree_case = np.any(p_distances[trees_indices] < state.radius[trees_indices] + game.staff_range)
        # noinspection PyTypeChecker
        if state.c_nearby_enemies == 0 and self.router.disabled and me.get_distance_to(*self.router.target) <= self.router.get_passed_distance(self.router.current_hardpoint):
            move.speed, move.strafe_speed = 0.0, 0.0
            move.turn = me.get_angle_to(*BASE_ENEMIES)
        elif state.c_nearby_enemies == 0 or (not nearest_enemy_dangerous and tree_case) or (nearest_enemy_dangerous and tree_case and health_factor < 0.5):
            distances = p_distances[trees_indices]
            angles = p_angles[trees_indices]
            # noinspection PyTypeChecker
            if np.any(distances < state.radius[trees_indices] + game.staff_range):
                # noinspection PyTypeChecker
                on_target = np.argwhere(distances < state.radius[trees_indices] + game.staff_range).ravel()
            else:
                on_target = ((distances < 0.75*me.cast_range) & (np.abs(angles) < self.attack_sector)).nonzero()[0]

            if on_target.size > 0:

                # t_index = state.p_coeffs[r][on_target].argmin()
                t_index = np.array([state.units[t_i].life for t_i in on_target]).argmin()  # distances[on_target].argmin()
                t_angle = p_angles[trees_indices][on_target][t_index]
                t_distance = p_distances[trees_indices][on_target][t_index]

                forbidden = neutrals + allies
                forbidden_angles_case = np.abs(p_angles[forbidden]) < self.attack_sector
                mm_case = (
                    (
                        p_distances[forbidden] <= t_distance
                    ) & (
                        forbidden_angles_case
                    )
                ).nonzero()[0].size == 0

                # noinspection PyUnresolvedReferences
                # st_case = forbidden_angles_case.nonzero()[0].size == 0

                target = state.units[trees_indices][on_target][t_index]
                angle_case = abs(t_angle) < self.attack_sector

                if can_use_staff and angle_case and t_distance < game.staff_range + state.radius[trees_indices][on_target][t_index]:
                    print state.tick, target.__class__.__name__, "STAFF"
                    move.action = ActionType.STAFF
                elif can_use_magic_missile and mm_case and angle_case and t_distance < me.cast_range:
                    print state.tick, target.__class__.__name__, "MAGIC_MISSILE"
                    move.cast_angle = t_angle
                    move.action = ActionType.MAGIC_MISSILE
                    move.min_cast_distance = t_distance - state.radius[trees_indices][on_target][t_index] + game.magic_missile_radius
                elif t_distance <= state.radius[trees_indices][on_target][t_index] + 1.25*me.radius:
                    move.turn = t_angle
        else:
            if p_distances[nearest_enemy] < 200.0:
                target = nearest_enemy
            elif can_use_shield and np.any(p_distances[allies] <= me.cast_range):
                if nearby_allies_wizards:
                    target = nearby_allies_wizards[
                        np.array([state.units[n_e].life for n_e in nearby_allies_wizards]).argmin()
                    ]
                else:
                    nearby_allies = [u_i for u_i in state.allies if initial_distances[u_i] <= me.cast_range]
                    target = nearby_allies[
                        np.array([state.units[n_e].life for n_e in nearby_allies]).argmin()
                    ]
            elif nearby_enemies_base and (float(state.units[nearby_enemies_base].life) < 80.0 or (not nearest_enemy_dangerous and not any_nearby_enemies_wizards_dangerous)):
                target = nearby_enemies_base
            elif fireball_target is not None:
                target = fireball_target
            elif nearby_enemies_wizards:
                target = nearby_enemies_wizards[
                    np.array([state.units[n_e].life for n_e in nearby_enemies_wizards]).argmin()
                ]
            elif any([float(state.units[n_e].life) / float(state.units[n_e].max_life) <= 0.3 for n_e in state.nearby_enemies]):
                shots = [n_e for n_e in state.nearby_enemies if float(state.units[n_e].life) / float(state.units[n_e].max_life) <= 0.3]
                target = shots[p_distances[shots].argmax()]
            else:
                # target = nearby_enemies[np.abs(state.p_coeffs[nearby_enemies].argmax())]
                target = state.nearby_enemies[
                    np.array([float(state.units[n_e].life) for n_e in state.nearby_enemies]).argmin()
                ]

            move.turn = p_angles[target]
            fireball_targets_count = np.count_nonzero(
                cdist(
                    state.positions[target, :].reshape((1, 2)),
                    state.positions[state.nearby_enemies, :]
                ) <= game.fireball_explosion_min_damage_range
            )

            if can_use_staff and p_distances[target] <= game.staff_range + state.radius[target]:
                print state.tick, state.units[target].__class__.__name__, "STAFF"
                move.action = ActionType.STAFF
                projectile_time = 0.0
            elif can_use_shield and target in allies:
                move.action = ActionType.SHIELD
                move.status_target_id = state.units[target].id
                print state.tick, state.units[target].__class__.__name__, state.units[target].id, "SHIELD"
                return
            elif can_use_fireball and (state.units_types[target] == Wizard or state.units_types[target] == Building
                                       or fireball_targets_count > 2
                                       or (fireball_targets_count > 1 and mana_factor > 0.9)) \
                and all(
                    [
                        distance_to_line(
                            me.x, me.y,
                            positions[target, 0], positions[target, 1],
                            positions[ti, 0], positions[ti, 1]
                        ) > state.radius[ti] + 1.3*game.fireball_radius
                        for ti in nearby_trees
                    ]
                ) and p_distances[target] > game.wizard_radius + 1.1*game.fireball_explosion_min_damage_range \
                    and state.units[target].life >= game.fireball_explosion_max_damage:

                # print state.tick, state.units[target].__class__.__name__, "FIREBALL"
                move.action = ActionType.FIREBALL
                move.min_cast_distance += game.fireball_radius
                projectile_time = p_distances[target] / game.fireball_speed
            elif can_use_frost_bolt and state.units[target].life >= game.frost_bolt_direct_damage \
                    and all(
                        [
                            distance_to_line(
                                me.x, me.y,
                                positions[target, 0], positions[target, 1],
                                positions[ti, 0], positions[ti, 1]
                            ) > state.radius[ti] + game.frost_bolt_radius
                            for ti in nearby_trees
                        ]
                    ) \
                    and (state.units_types[target] == Wizard or (mana_factor > 0.6)):
                # print state.tick, state.units[target].__class__.__name__, "FROST_BOLT"
                move.action = ActionType.FROST_BOLT
                move.min_cast_distance += game.frost_bolt_radius
                projectile_time = p_distances[target] / game.frost_bolt_speed
            elif can_use_magic_missile:
                # print state.tick, state.units[target].__class__.__name__, "MAGIC_MISSILE"
                move.action = ActionType.MAGIC_MISSILE
                move.min_cast_distance += game.magic_missile_radius
                projectile_time = p_distances[target] / game.magic_missile_speed
            else:
                projectile_time = np.inf

            px = state.units[target].x + 0.4*projectile_time*state.units[target].speed_x
            py = state.units[target].y + 0.4*projectile_time*state.units[target].speed_y
            angle, distance = me.get_angle_to(px, py), me.get_distance_to(px, py)
            # angle, distance = float(move.turn), p_distances[target]
            if abs(angle) > self.attack_sector or distance > me.cast_range + state.radius[target] or projectile_time == np.inf:
                move.action = ActionType.NONE
                move.min_cast_distance = 0.0
            elif projectile_time == 0.0:
                pass
            else:
                move.cast_angle = angle
                # move.max_cast_distance = float(move.min_cast_distance + distance)
                move.min_cast_distance += distance - state.radius[target]

        if abs(abs(move.turn) - np.pi) <= 0.0006:
            if enemies:
                move.turn = me.get_angle_to_unit(state.units[enemies[state.p_distances[enemies].argmin()]])
            else:
                move.turn = me.get_angle_to(*self.router.target)

        if state.tick % 60 == 0:
            ct = time()
            print state.tick, self.router.current_hardpoint, ct - t0, k, me.level, me.skills, ct - self.t0

        if debug:
            with debug.pre() as dbg:
                self.router.plot(dbg, state)