import numpy as np

from model.Game import Game
from model.Wizard import Wizard
from model.Projectile import Projectile
from model.SkillType import SkillType
from model.Tree import Tree
from model.StatusType import StatusType

from utils import pi_half, linear_interpolation


__all__ = (
    'Potential',
    'EmptyPotential',
    'EnemyBlowdartPotential',
    'EnemyOrcPotential',
    'EnemyMinionStaffPotential',
    'SkinPotential',
    'EnemyWizardPotential',
    'EnemyWizardPotentialOff',
    'EnemiesBasePotential',
    'EnemiesBasePotentialOff',
    'EnemyTowerPotential',
    'EnemyTowerPotentialStaff',
    'EnemyTowerPotentialIn',
    'ProjectilePotential',
    'TreePotential'
)


BASE_ATTRACTION_FACTOR = -0.00001


class Potential(object):

    def __call__(self, d, abs_angle):
        """
        :type d: float
        :type abs_angle: float
        """
        raise NotImplementedError()


class EmptyPotential(Potential):

    def update(self, *args):
        pass

    def __call__(self, d, abs_angle):
        """
        :type d: float
        :type abs_angle: float
        """
        return 0.0


class EnemyMinionStaffPotential(Potential):

    inner_attraction = 5.0 * BASE_ATTRACTION_FACTOR
    attraction = 2.0 * BASE_ATTRACTION_FACTOR

    def __init__(self, game):
        """
        :type game: Game
        """
        self.radius = game.minion_radius + game.staff_range
        self.attraction_radius = 0.6*game.score_gain_range

    def __call__(self, d, abs_angle):
        if d <= self.radius:
            return linear_interpolation(d, 0.0, self.radius, self.inner_attraction, self.attraction)
        # elif d < self.outer_radius:
        #     return linear_interpolation(d, self.radius, self.outer_radius, 1.0, 0.0)
        elif d < self.attraction_radius:
            return linear_interpolation(d, self.attraction_radius, self.radius, 0.0, self.attraction)
        return 0.0


class EnemyMinionPotential(Potential):

    def __init__(self, sector, inner, outer, game):
        """
        :type game: Game
        """
        super(EnemyMinionPotential, self).__init__()
        self.sector = sector
        self.inner_radius = inner
        self.outer_radius = outer
        self.score_range = game.score_gain_range

    def __call__(self, d, abs_angle):
        """
        :type d: float
        :type abs_angle: float
        :rtype: float
        """
        if d >= self.score_range:
            return 0.0
        if abs_angle <= self.sector:
            k_angle = linear_interpolation(abs_angle, 0.0, self.sector, 5.0, 1.0)
        else:
            k_angle = linear_interpolation(abs_angle, self.sector, np.pi, 1.0, 0.0)
        if d < self.inner_radius:
            return linear_interpolation(d, 0.0, self.inner_radius, 80.0, 50.0)
        elif d < self.outer_radius:
            return k_angle * linear_interpolation(d, self.inner_radius, self.outer_radius, 50.0, 0.0)
        # elif d < self.score_range:
        #     return linear_interpolation(d, self.score_range, self.outer_radius, 0.0, BASE_ATTRACTION_FACTOR)
        return 0.0


class EnemyOrcPotential(EnemyMinionPotential):

    def __init__(self, game):
        """
        :type game: Game
        """
        inner = game.orc_woodcutter_attack_range + game.wizard_radius + 20.0
        super(EnemyOrcPotential, self).__init__(
            sector=0.65*game.orc_woodcutter_attack_sector,
            inner=inner,
            outer=inner + 60.0,
            game=game
        )


class EnemyBlowdartPotential(EnemyMinionPotential):

    def __init__(self, game):
        """
        :type game: Game
        """
        inner = game.fetish_blowdart_attack_range + game.wizard_radius + game.dart_radius + 20.0
        super(EnemyBlowdartPotential, self).__init__(
            sector=0.65*game.fetish_blowdart_attack_sector,
            inner=inner,
            outer=inner + 25.0,
            game=game
        )


class EnemyWizardPotentialOff(Potential):

    def __init__(self, wizard, game, health_factor, me):
        """
        :type wizard: Wizard
        :type game: Game
        :type health_factor: float
        :type me: Wizard
        """
        super(EnemyWizardPotentialOff, self).__init__()
        self.cast_sector = 0.6*game.staff_sector
        self.wizard_radius = game.wizard_radius
        self.cast_range = wizard.cast_range
        self.safe_range = self.cast_range + 2.0*game.wizard_radius
        self.attack_range = me.cast_range + 0.8*game.wizard_radius
        if float(wizard.life) / float(wizard.max_life) <= 0.5 < health_factor:
            self.safe_range = self.attack_range
        elif SkillType.FROST_BOLT in wizard.skills:
            self.safe_range += game.frost_bolt_radius
        elif SkillType.FIREBALL in wizard.skills:
            self.safe_range += game.fireball_explosion_max_damage_range
        else:
            self.safe_range += game.magic_missile_radius

        self.can_soot = wizard.remaining_action_cooldown_ticks == 0
        if health_factor < 0.7:
            self.attack_range = float(self.safe_range)
            self.safe_range *= 1.3
        elif StatusType.FROZEN in wizard.statuses or health_factor >= 0.9:
            self.safe_range = float(self.attack_range)
            self.can_soot = False

    def __call__(self, d, abs_angle):
        if d <= 2.0*self.wizard_radius:
            return 100.0
        if abs_angle <= self.cast_sector:
            r = self.safe_range
            k_angle = linear_interpolation(abs_angle, 0.0, self.cast_sector, 10.0, 2.0)
        else:
            r = self.attack_range
            k_angle = linear_interpolation(abs_angle, self.cast_sector, pi_half, 2.0, 1.0)

        r2 = 0.85 * r

        if d < r2:
            return k_angle*linear_interpolation(d, self.wizard_radius, r2, 100.0, 60.0)
        elif d < r:
            return k_angle*linear_interpolation(d, r2, r, 60.0, 0.0)
        return 0.0


class EnemyWizardPotential(EnemyWizardPotentialOff):

    def __call__(self, d, abs_angle):
        if d <= 2.0*self.wizard_radius:
            return 100.0
        if abs_angle <= self.cast_sector:
            r = self.safe_range
            k_angle = linear_interpolation(abs_angle, 0.0, self.cast_sector, 10.0, 2.0)
        else:
            r = self.attack_range
            k_angle = linear_interpolation(abs_angle, self.cast_sector, pi_half, 2.0, 1.0)

        r2 = 0.85 * r
        skin = 1.05 * r

        if d < r2:
            return k_angle*linear_interpolation(d, self.wizard_radius, r2, 100.0, 60.0)
        elif d < r:
            return k_angle*linear_interpolation(d, r2, r, 60.0, 0.0)
        elif d < skin:
            return linear_interpolation(d, r, skin, BASE_ATTRACTION_FACTOR, 0.0)
        return 0.0


class EnemyTowerPotential(Potential):

    def __init__(self, game):
        """
        :type game: Game
        """
        super(EnemyTowerPotential, self).__init__()
        self.cast_range = float(game.guardian_tower_attack_range)
        self.safe_range = self.cast_range + game.wizard_radius

    def __call__(self, d, abs_angle):
        """
        :type d: float
        :type abs_angle: float
        """
        if d <= self.safe_range:
            return linear_interpolation(d, self.cast_range, self.safe_range, 100.0, 0.0)
        return 0.0


class EnemyTowerPotentialStaff(Potential):

    attraction = 20.0 * BASE_ATTRACTION_FACTOR

    def __init__(self, game):
        self.r = game.guardian_tower_radius + 0.75*game.staff_range
        self.radius = game.score_gain_range

    def __call__(self, d, abs_angle):
        """
        :type d: float
        :type abs_angle: float
        """
        if d < self.r:
            return linear_interpolation(d, 0.0, self.r, 1.0, 0.0)
        if self.r <= d <= self.radius:
            return linear_interpolation(d, self.r, self.radius, self.attraction, 0.0)
        return 0.0


class EnemyTowerPotentialIn(Potential):

    def __init__(self, game):
        self.r = 0.8*game.score_gain_range
        self.radius = 1.2*game.score_gain_range

    def __call__(self, d, abs_angle):
        """
        :type d: float
        :type abs_angle: float
        """
        if self.r <= d <= self.radius:
            return linear_interpolation(d, self.r, self.radius, BASE_ATTRACTION_FACTOR, 0.0)
        return 0.0


class SkinPotential(Potential):

    def __init__(self, radius, skin, k1=1.0, k2=0.0):
        self.radius = radius
        self.outer_radius = radius + skin
        self.k1 = k1
        self.k2 = k2

    def __call__(self, d, abs_angle):
        """
        :type d: float
        :type abs_angle: float
        """
        if d <= self.radius:
            return self.k1
        if d < self.outer_radius:
            return linear_interpolation(d, self.radius, self.outer_radius, self.k1, self.k2)
        return 0.0


class TreePotential(Potential):

    def __init__(self, tree, game, base_potential):
        """
        :type tree: Tree
        """
        self.radius = tree.radius + game.wizard_radius
        self.outer_radius = 1.01 * self.radius
        self.k = 0.0005*abs(base_potential)

    def __call__(self, d, abs_angle):
        if d < self.radius:
            return self.k
        if d < self.outer_radius:
            return linear_interpolation(d, self.radius, self.outer_radius, self.k, 0.0)
        return 0.0


class EnemiesBasePotential(Potential):

    def __init__(self, game):
        """
        :type game: Game
        """
        super(EnemiesBasePotential, self).__init__()
        self.inner_radius = game.faction_base_radius + game.wizard_radius
        self.attraction_radius = 0.5*game.score_gain_range
        self.radius = 1.25*game.score_gain_range

    def __call__(self, d, abs_angle):
        """

        :type d: float
        :type abs_angle: float
        """
        if d < self.inner_radius:
            return 1.0
        elif d >= self.attraction_radius:
            return linear_interpolation(d, self.radius, self.attraction_radius, 0.0, BASE_ATTRACTION_FACTOR)
        return 0.0


class EnemiesBasePotentialOff(Potential):

    def __init__(self, game):
        """
        :type game: Game
        """
        self.inner_radius = game.faction_base_radius + game.wizard_radius
        self.outer_radius = game.faction_base_attack_range + game.wizard_radius + 50.0

    def __call__(self, d, abs_angle):
        """

        :type d: float
        :type abs_angle: float
        """
        if d < self.inner_radius:
            return 10.0
        elif d < self.outer_radius:
            return linear_interpolation(d, self.inner_radius, self.outer_radius, 100.0, 0.0)
        return 0.0


class ProjectilePotential(Potential):

    def __init__(self, projectile, game):
        """
        :type projectile: Projectile
        :type game: Game
        """
        self.radius = projectile.radius

    def __call__(self, d, abs_angle):
        """
        :type d: float
        :type abs_angle: float
        """
        if d <= self.radius:
            return 1.0
        return 0.0
