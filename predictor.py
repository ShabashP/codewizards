# coding=utf-8
from model.Wizard import Wizard
from model.Minion import Minion
from model.Projectile import Projectile
from model.ProjectileType import ProjectileType
from model.Game import Game

from collections import defaultdict
from itertools import product
import numpy as np

from scipy.spatial.distance import pdist, squareform
from scipy.interpolate import interp1d

from state import WorldState

from utils import angle_to_point, linear_interpolation, angle_to_point_scalar

DEPTH = 5
GRID_SECTORS = 11


class Predictor(object):

    sim_units_types = (Projectile, Minion, Wizard)

    def __init__(self, game):
        """
        :type game: Game
        """

        self.min_speed, self.max_speed = -game.wizard_backward_speed, game.wizard_forward_speed
        self.min_strafe_speed, self.max_strafe_speed = -game.wizard_strafe_speed, game.wizard_strafe_speed
        self.min_turn, self.max_turn = -game.wizard_max_turn_angle, game.wizard_max_turn_angle

        speed_range = [self.min_speed, 0.5 * self.min_speed, 0.0, 0.5 * self.max_speed, self.max_speed]
        strafe_speed_range = np.linspace(-self.max_strafe_speed, self.max_strafe_speed, 5)

        controls = []
        for speed, strafe_speed in product(speed_range, strafe_speed_range):
            speed_ratio = speed / self.max_speed if speed > 0 else speed / self.min_speed
            strafe_speed_ratio = strafe_speed / self.max_strafe_speed

            h = np.sqrt(speed_ratio ** 2 + strafe_speed_ratio ** 2)
            if h <= 1.0:
                controls.append((speed, strafe_speed))
            else:
                controls.append((speed / h, strafe_speed / h))
        controls = np.array(controls)

        # noinspection PyTypeChecker
        ind = np.argwhere(controls[:, 1] >= 0.0).ravel()
        approx = {}
        for x, y in controls[ind]:
            if x in approx and y < approx[x][0]:
                continue
            approx[x] = (
                y,
                np.arctan2(y, x),
                np.hypot(x, y)
            )
        approx_data = np.array(approx.values())[:, 1:]

        self.speed_interpolation = interp1d(approx_data[:, 0], approx_data[:, 1])

        axis = np.zeros((GRID_SECTORS-1, 3))
        axis[:, 0] = np.linspace(-np.pi, np.pi, GRID_SECTORS)[:-1]
        axis[:, 1] = np.cos(axis[:, 0])
        axis[:, 2] = np.sin(axis[:, 0])
        # print np.rad2deg(axis[:, 0])
        self.controls = np.zeros((axis.shape[0], 2))
        self.offsets = np.zeros((axis.shape[0],))
        for i in xrange(axis.shape[0]):
            angle, ax, ay = axis[i]
            speed_vec = self.speed_interpolation(abs(angle))
            self.controls[i, 0] = speed_vec * ax
            self.controls[i, 1] = speed_vec * ay
            self.offsets[i] = speed_vec
        self.c_controls = self.controls.shape[0]
        self.max_offset = DEPTH * (self.max_speed + game.minion_speed) + game.wizard_radius + game.minion_radius

    def __call__(self, state, game, router):
        """
        :type state: WorldState
        :type game: Game
        :type router: Router
        """
        if state.c_scan_units == 0:
            return self._just_me(state, game, router)
        return self._simulate(state, game, router)

    def _just_me(self, state, game, router):
        """
        :type state: WorldState
        :type game: Game
        :type router: Router
        """
        depth = int(np.floor(float(DEPTH)/2.0))
        grid = np.zeros((self.c_controls, 2, depth))
        grid[:, 0, 0] = state.player.x
        grid[:, 1, 0] = state.player.y
        grid_potentials = np.zeros((self.c_controls,))

        for t in xrange(1, depth):
            for i in xrange(self.c_controls):
                speed, strafe_speed = self.controls[i]

                x = grid[i, 0, t - 1] + speed * state.ax - strafe_speed * state.ay
                y = grid[i, 1, t - 1] + speed * state.ay + strafe_speed * state.ax

                grid_potentials[i] += router.get_potential(x, y)
                grid[i, :, t] = [x, y]
        m = grid_potentials.argmin()
        return self.controls[m], grid[m, :, :], [], 0, []

    def _simulate(self, state, game, router):
        """
        :type state: WorldState
        :type game: Game
        :type router: Router
        """
        depth = int(np.floor(float(DEPTH) / 2.0)) if state.c_nearby_enemies == 0 else DEPTH
        grid = np.zeros((self.c_controls, 2, depth))
        grid[:, 0, 0] = state.player.x
        grid[:, 1, 0] = state.player.y
        grid_potentials = np.zeros((self.c_controls,))
        collisions = defaultdict(set)
        k = 0

        predicated = np.zeros((state.c_scan_units, 2, depth))
        distances = np.zeros((self.c_controls, state.c_scan_units, depth))

        for i in xrange(self.c_controls):
            predicated[:, :, 0] = state.positions[state.scan_units, :]

        for t in xrange(1, depth):

            # u_distances = squareform(pdist(predicated[:, :, t - 1]))

            # Предсказание перемещения юнитов без учета перемещения игрока
            # -----------------------------------------------------------------------------------------------------
            for j in xrange(state.c_scan_units):
                u_i = state.scan_units[j]
                if u_i not in state.sim_units or state.units_types[u_i] not in self.sim_units_types:
                    predicated[j, :, t] = predicated[j, :, t - 1]
                    continue

                if predicated[j, 0, t - 1] == np.nan:
                    predicated[j, :, t] = np.nan
                    continue

                # u_c_distance = u_distances[j] - state.radius[u_i] - state.radius[state.scan_units]
                #
                # # noinspection PyTypeChecker
                # u_collisions = np.where(np.argwhere(u_c_distance <= 0.0).ravel() != j)[0]
                #
                # # noinspection PyTypeChecker
                # if u_collisions.size > 0 and state.units_types[u_i] is Projectile \
                #         and (state.units[u_i].type == ProjectileType.DART or u_i not in dangerous_projectiles):
                #     predicated[j, :, t] = np.nan
                #     continue

                predicated[j, :, t] = predicated[j, :, t - 1] + state.speed[u_i]

            # Перемещения моего игрока
            # -----------------------------------------------------------------------------------------------------
            for i in xrange(self.c_controls):

                speed, strafe_speed = self.controls[i]

                grid[i, 0, t] = grid[i, 0, t - 1] + speed * state.ax - strafe_speed * state.ay
                grid[i, 1, t] = grid[i, 1, t - 1] + speed * state.ay + strafe_speed * state.ax

                if not (state.player.radius < grid[i, 0, t] < game.map_size
                        and state.player.radius < grid[i, 1, t] < game.map_size):
                    grid[i, :, t] = grid[i, :, t - 1]

                distances[i, :, t] = np.hypot(predicated[:, 0, t] - grid[i, 0, t], predicated[:, 1, t] - grid[i, 1, t])

                p_collisions = distances[i, :, t] - state.radius[state.scan_units] - state.player.radius < 0.0

                # noinspection PyTypeChecker
                for j in np.argwhere(p_collisions).ravel():

                    u_i = state.scan_units[j]

                    if state.units_types[u_i] != Projectile:
                        grid[i, :, t] = grid[i, :, t - 1]
                        continue
                    if u_i not in collisions:
                        collisions[i].add(u_i)
                        grid_potentials[i] += state.potentials[u_i](
                            distances[i, j, t],
                            np.abs(
                                angle_to_point_scalar(
                                    predicated[j, 0, t],
                                    predicated[j, 1, t],
                                    state.angles[u_i],
                                    grid[i, 0, t],
                                    grid[i, 1, t]
                                )
                            )
                        )
                        # print "%s (%s) Predicated %s projectile %s (%s) and player collision" % (
                        #     state.tick, i, t + state.tick, state.units[u_i].id, state.units[u_i].type
                        # )

                grid_potentials[i] += router.get_potential(
                    grid[i, 0, t],
                    grid[i, 1, t]
                )

        # Потенциал в начальной точке
        p0, p1 = 0.0, np.zeros((self.c_controls,))
        angles = np.abs(
            angle_to_point(
                x=predicated[:, 0, 0],
                y=predicated[:, 1, 0],
                angle=state.angles[state.scan_units],
                x1=state.player.x,
                y1=state.player.y
            )
        )
        for j, u_i in enumerate(state.scan_units):
            p0 += state.potentials[u_i](state.p_distances[u_i], angles[j])
            k += 1

        # Потенциалы в конечных точках каждой ветки
        for i in xrange(self.c_controls):
            angles = np.abs(
                angle_to_point(
                    x=predicated[:, 0, -1],
                    y=predicated[:, 1, -1],
                    angle=state.angles[state.scan_units],
                    x1=grid[i, 0, -1],
                    y1=grid[i, 1, -1]
                )
            )
            for j, u_i in enumerate(state.scan_units):
                ppp = state.potentials[u_i](distances[i, j, -1], angles[j])
                p1[i] += ppp
                k += 1

        # Интерполируем потенциалы в остальных точках
        for t in xrange(1, depth - 1):
            for i in xrange(self.c_controls):
                grid_potentials[i] += linear_interpolation(
                    self.offsets[i] * t,
                    0.0, self.offsets[i] * depth,
                    p0, p1[i]
                )

        grid_potentials += p1

        m = grid_potentials.argmin()

        return self.controls[m], grid[m, :, :], predicated, k, distances[m, :, :]
