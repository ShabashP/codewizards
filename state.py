class WorldState(object):

    def __init__(self,
                 tick,
                 player,
                 player_damage_per_tick,
                 ax, ay,
                 units,
                 units_types,
                 potentials,
                 p_distances,
                 p_angles,
                 radius,
                 positions,
                 scan_range,
                 scan_units,
                 c_scan_units,
                 sim_units,
                 c_sim_units,
                 speed,
                 angles,
                 allies,
                 enemies,
                 neutrals,
                 u_damage_per_tick):
        self.tick = tick
        self.player = player
        self.player_damage_per_tick = player_damage_per_tick
        self.ax, self.ay = ax, ay
        self.units = units
        self.units_types = units_types
        self.potentials = potentials
        self.p_distances = p_distances
        self.p_angles = p_angles
        self.radius = radius
        self.positions = positions
        self.scan_range = scan_range
        self.scan_units = scan_units
        self.c_scan_units = c_scan_units
        self.sim_units = sim_units
        self.c_sim_units = c_sim_units
        self.allies = allies
        self.enemies = enemies
        self.neutrals = neutrals
        self.speed = speed
        self.angles = angles
        self.u_damage_per_tick = u_damage_per_tick

        self.nearby_enemies = [
            u_i for u_i in enemies if p_distances[u_i] <= player.cast_range + self.radius[u_i]
        ]
        self.c_nearby_enemies = len(self.nearby_enemies)