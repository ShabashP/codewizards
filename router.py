# coding=utf-8
import numpy as np
from collections import defaultdict, deque
from itertools import product
from scipy.spatial.distance import cdist, pdist, squareform
from scipy.sparse.csgraph import csgraph_from_dense, dijkstra

from model.World import World
from model.Game import Game
from model.Wizard import Wizard
from model.Minion import Minion
from model.Building import Building
from model.LaneType import LaneType

from state import WorldState
from potential import BASE_ATTRACTION_FACTOR
from utils import distance_to_line


UNITS_WEIGHT = {
    Wizard: 2.5,
    Building: 1.75,
    Minion: 1.0
}


# Бонусы
BONUS_TOP = (1200.0, 1200.0)
BONUS_BOTTOM = (2800.0, 2800.0)
BONUSES_LOCATION = np.array(
    [
        list(BONUS_TOP),
        list(BONUS_BOTTOM)
    ], dtype=np.float
)
BONUSES = (BONUS_TOP, BONUS_BOTTOM)

# Координаты баз фракций
BASE_ALLIES = (400.0, 3600.0)
BASE_ENEMIES = (3600.0, 400.0)

HARDPOINT_PASSED_DISTANCE = 120.0
HARDPOINT_SCAN_DISTANCE = 250.0
HARDPOINT_SCAN_INTERVAL = 100

INITIAL_HARDPOINT = {
    LaneType.TOP: 11,
    LaneType.BOTTOM: 5,
    LaneType.MIDDLE: 17
}
BASE_HARDPOINT_WEIGHT = -1.0
BASE_MARGIN = 400.0
LANE_MARGIN = 1200.0


class Router(object):

    hp_base_attraction = 0.75 * BASE_ATTRACTION_FACTOR
    hp_bonus_attraction = 100.0 * BASE_ATTRACTION_FACTOR

    def __init__(self, game, wizard, lane=None):
        """
        :type game: Game
        :type wizard: Wizard
        :type lane: int
        """

        self.lanes = {
            LaneType.MIDDLE: [],
            LaneType.TOP: [],
            LaneType.BOTTOM: []
        }
        self.paths = defaultdict(list)

        offset = np.linspace(0.5 * BASE_MARGIN, game.map_size - 0.5 * BASE_MARGIN, 10, endpoint=True)
        waypoints = []

        d_bonus = distance_to_line(0.0, game.map_size, game.map_size, 0.0, *BONUS_TOP)

        for i, waypoint in enumerate(product(offset, offset)):
            x, y = waypoint
            if x <= LANE_MARGIN or y <= LANE_MARGIN:
                self.lanes[LaneType.TOP].append(i)
            if x >= game.map_size - LANE_MARGIN or y >= game.map_size - LANE_MARGIN:
                self.lanes[LaneType.BOTTOM].append(i)
            if distance_to_line(0.0, 4000.0, 4000.0, 0.0, x, y) <= d_bonus:
                self.lanes[LaneType.MIDDLE].append(i)

            waypoints.append((x, y))

        waypoints.append(BONUS_TOP)
        waypoints.append(BONUS_BOTTOM)
        waypoints.append(BASE_ENEMIES)
        waypoints.append(BASE_ALLIES)

        self.index_enemies_base = waypoints.index(BASE_ENEMIES)
        self.index_allies_base = waypoints.index(BASE_ALLIES)
        self.index_top_bonus = waypoints.index(BONUS_TOP)
        self.index_bottom_bonus = waypoints.index(BONUS_BOTTOM)
        self.bonuses_indices = (
            self.index_bottom_bonus,
            self.index_top_bonus
        )

        self.hardpoints = np.array(waypoints)

        self.active_hardpoints = self.lanes.get(lane, range(self.hardpoints.shape[0]))
        self.active_hardpoints.extend((
            self.index_allies_base,
            self.index_enemies_base,
        ))
        if lane == LaneType.TOP:
            self.active_hardpoints.append(self.index_top_bonus)
        elif lane == LaneType.BOTTOM:
            self.active_hardpoints.append(self.index_bottom_bonus)
        else:
            self.active_hardpoints.extend((
                self.index_bottom_bonus,
                self.index_top_bonus
            ))

        self.size = self.hardpoints.shape[0]

        dense = np.zeros((self.size, self.size))

        d = squareform(pdist(self.hardpoints))
        for i, j in product(self.active_hardpoints, self.active_hardpoints):
            if d[i, j] < 1.5*BASE_MARGIN:
                dense[i, j] = d[i, j]
                self.paths[i].append(j)
                self.paths[j].append(i)

        dense = np.ma.masked_values(dense, 0)
        distances, self.predecessors = dijkstra(
            csgraph_from_dense(dense), directed=False, return_predecessors=True
        )

        self.got_bonus_distance = game.wizard_radius + game.bonus_radius

        self._hardpoint_passed_distances = {
            self.index_top_bonus: game.wizard_radius + game.bonus_radius,
            self.index_bottom_bonus: game.wizard_radius + game.bonus_radius,
        }
        self._hardpoint_scan_distances = {}

        self.base_weight = {}

        self.weight = np.zeros((self.size,))

        # noinspection PyTypeChecker
        self.distances = np.hypot(
            self.hardpoints[:, 0] - wizard.x,
            self.hardpoints[:, 1] - wizard.y,
        )

        self.times = np.zeros_like(self.distances)
        self.prev_hardpoints = deque([], maxlen=2)

        self.tick = 0
        self.world_size = game.map_size
        self.wizard_radius = game.wizard_radius
        self.bonus_cost = float(game.bonus_score_amount)
        if not game.skills_enabled:
            self.bonus_cost *= 1.5
        if game.raw_messages_enabled and lane == LaneType.MIDDLE:
            self.bonus_cost *= 0.1
        self.bonus_appearance_interval = game.bonus_appearance_interval_ticks
        self.bonus_next_appearance_time = self.bonus_appearance_interval
        self.bonuses = [False, False]
        self.updated = 0

        self.route = deque(self.restore_path(
            self.distances.argmin(),
            INITIAL_HARDPOINT.get(lane, np.random.choice(self.active_hardpoints))
        ))
        print "Initial route", self.route

    @property
    def current_hardpoint(self):
        return self.route[0]

    @property
    def target(self):
        return self.hardpoints[self.current_hardpoint, :]

    @property
    def hp_attraction(self):
        if self.current_hardpoint in self.bonuses_indices:
            return self.hp_bonus_attraction
        return self.hp_base_attraction

    def get_passed_distance(self, hp_index):
        return self._hardpoint_passed_distances.get(hp_index, HARDPOINT_PASSED_DISTANCE)

    def get_scan_distance(self, hp_index):
        return self._hardpoint_scan_distances.get(hp_index, HARDPOINT_SCAN_DISTANCE)

    @property
    def disabled(self):
        return len(self.route) == 1 and self.distances[self.current_hardpoint] <= self.get_passed_distance(self.current_hardpoint)

    def plot(self, visualizer, state):

        for i in self.active_hardpoints:
            # axis.plot(self.hardpoints[i, 0], self.hardpoints[i, 1], 'ko', markersize=8.0)
            visualizer.text(
                self.hardpoints[i, 0] + 30.0,
                self.hardpoints[i, 1],
                "%s   %3.4f" % (i, self.weight[i]),
                (0, 0, 0)
            )
            visualizer.circle(
                self.hardpoints[i, 0],
                self.hardpoints[i, 1],
                self.get_passed_distance(i),
                color=(0, 0, 0) if i == self.current_hardpoint else (187.0/256.0, 195.0/256.0, 206.0/256.0)
            )
            visualizer.circle(
                self.hardpoints[i, 0],
                self.hardpoints[i, 1],
                self.get_scan_distance(i),
                color=(167.0/256.0, 181.0/256.0, 204.0/256.0)
            )

            for j in self.paths[i]:
                visualizer.line(
                    self.hardpoints[i, 0], self.hardpoints[i, 1],
                    self.hardpoints[j, 0], self.hardpoints[j, 1],
                    color=(187.0 / 256.0, 195.0 / 256.0, 206.0 / 256.0)
                )

        # xd, yd = self.hardpoints[self.current_hardpoint]
        # axis.plot(xd, yd, 'kx', markersize=16.0)
        for i in xrange(len(self.route) - 1):
            visualizer.line(
                self.hardpoints[self.route[i], 0], self.hardpoints[self.route[i], 1],
                self.hardpoints[self.route[i+1], 0], self.hardpoints[self.route[i+1], 1],
                color=(0.0, 0.0, 0.0)
            )
        # axis.plot(
        #     (state.player.x, self.hardpoints[self.route[0], 0]),
        #     (state.player.y, self.hardpoints[self.route[0], 1]),
        #     'k-', linewidth=1.5
        # )

    def restore_path(self, index_from, index_to):
        assert 0 <= index_from < self.size, str(index_from)
        assert 0 <= index_to < self.size, str(index_to)
        path = []
        i = index_to
        while i != index_from:
            path.append(i)
            i = self.predecessors[index_from, i]

        path.append(index_from)
        return path[::-1]

    def calc_weight(self, state, game):
        """
        :type state: WorldState
        :type game: Game
        """

        # noinspection PyTypeChecker
        weight = np.zeros((self.size,))
        nearby = defaultdict(lambda: [[], []])

        d = cdist(
            self.hardpoints,
            state.positions
        )

        # 0 E_life | 1 E_damage_per_tick | 2 A_life | 3 A_damage_per_tick | 4 kill_score_bonus | 5 E_weight | 6 A_weight
        fight_data = np.zeros((self.size, 7))
        for i in xrange(self.size):

            if i not in self.active_hardpoints:
                continue

            # noinspection PyTypeChecker
            indices = np.argwhere(d[i] <= self.get_scan_distance(i)).ravel()

            for j in indices:
                if j in state.allies:
                    nearby[i][1].append(j)
                    fight_data[i][2] += float(state.units[j].life)
                    fight_data[i][3] += state.u_damage_per_tick[j]
                    fight_data[i][6] += UNITS_WEIGHT.get(state.units_types[j], 0.0)
                elif j in state.enemies:
                    if state.units_types[j] == Minion:
                        fight_data[i, 4] += float(game.minion_elimination_score_factor) * float(state.units[j].max_life)
                    elif state.units_types[j] == Wizard:
                        fight_data[i, 4] += float(game.wizard_elimination_score_factor) * float(state.units[j].max_life)
                    elif state.units_types[j] == Building:
                        fight_data[i, 4] += float(game.building_elimination_score_factor) * float(state.units[j].max_life)

                    nearby[i][0].append(j)
                    fight_data[i][0] += float(state.units[j].life)
                    fight_data[i][1] += state.u_damage_per_tick[j]
                    fight_data[i][5] += UNITS_WEIGHT.get(state.units_types[j], 0.0)

            if i == self.index_top_bonus and (self.bonuses[0] or state.tick + self.times[self.index_top_bonus] >= self.bonus_next_appearance_time):
                    weight[i] += game.bonus_score_amount * np.exp(-0.0025*self.distances[i])
            elif i == self.index_bottom_bonus and (self.bonuses[1] or state.tick + self.times[self.index_bottom_bonus] >= self.bonus_next_appearance_time):
                    weight[i] += game.bonus_score_amount * np.exp(-0.0025*self.distances[i])

            enemies, allies = nearby[i]
            if not enemies:
                continue

            e_life, e_damage_per_tick, a_life, a_damage_per_tick, kill_score_bonus, e_weight, a_weight = fight_data[i, :]

            a_weight += UNITS_WEIGHT.get(Wizard, 1.0)
            a_life += state.player.life
            a_damage_per_tick += state.player_damage_per_tick

            # Оценка времени битвы
            tf = min((e_weight * e_life / a_damage_per_tick, a_weight * a_life / e_damage_per_tick))

            # Оценка времени движения до точки
            if self.times[i] >= tf:
                continue

            # Вероятность выиграть
            ta = a_weight * a_life * a_damage_per_tick
            v = ta * self.base_weight.get(i, 1.0) * np.exp(-0.0025*self.distances[i]) / (e_weight*e_life*e_damage_per_tick + ta)

            if not allies:
                v /= 100.0

            a_wizards = 1
            for kk in allies:
                if state.units_types[kk] == Wizard:
                    a_wizards += 1

            # Оценка моих очков
            score_damage = state.player_damage_per_tick * e_life / a_damage_per_tick
            score_kill = kill_score_bonus if a_wizards == 1 \
                else game.team_working_score_factor*kill_score_bonus / a_wizards

            if i == self.index_enemies_base:
                score_damage += game.victory_score

            # noinspection PyUnresolvedReferences
            weight[i] += v * (score_damage + score_kill)

            # print i, v, score_damage, score_kill, a_wizards, tf / self.times[i]

        return weight

    def get_potential(self, x, y):

        if not (self.wizard_radius < x < self.world_size - self.wizard_radius):
            return np.inf
        if not (self.wizard_radius < y < self.world_size - self.wizard_radius):
            return np.inf

        p = np.hypot(
            self.hardpoints[self.route[0], 0] - x,
            self.hardpoints[self.route[0], 1] - y,
        )

        if p < 1.0:
            return BASE_ATTRACTION_FACTOR  #-np.exp(-0.0005*self.hp_passed_distance)

        return self.hp_attraction / p  #-np.exp(-0.0005*p)

    def __call__(self, state, game, predictor, world, health_factor):
        """
        :type state: WorldState
        :type game: Game
        :type predictor: Predictor
        :type world: World
        :type health_factor: float
        """

        should_update = state.tick >= self.updated + HARDPOINT_SCAN_INTERVAL

        self.distances = np.hypot(
            self.hardpoints[:, 0] - state.player.x,
            self.hardpoints[:, 1] - state.player.y,
        )

        self.times = self.distances / predictor.speed_interpolation(
            np.abs([state.player.get_angle_to(*self.hardpoints[i]) for i in xrange(self.size)])
        )

        if self.tick > 0 and any(
                (tick % self.bonus_appearance_interval == 0 for tick in xrange(self.tick, state.tick))
        ):
            self.bonuses = [True, True]
            self.bonus_next_appearance_time += self.bonus_appearance_interval
            should_update = True
            print "Tick %s,  Bonus appear, next: %s" % (state.tick, self.bonus_next_appearance_time)

        for bonus in world.bonuses:
            if (bonus.x, bonus.y) == BONUS_TOP:
                # print "Tick %s ensured bonus top" % self.tick
                self.bonuses[0] = True
            elif (bonus.x, bonus.y) == BONUS_BOTTOM:
                # print "Tick %s ensured bonus bottom" % self.tick
                self.bonuses[1] = True

        if self.times[self.index_top_bonus] + state.tick < self.bonus_next_appearance_time:
            should_update = True

        if self.times[self.index_bottom_bonus] + state.tick < self.bonus_next_appearance_time:
            should_update = True

        bonuses_distances = cdist(
            BONUSES_LOCATION,
            np.array([[w.x, w.y] for w in world.wizards])
        )

        got_top_bonus_case = world.wizards[bonuses_distances[0, :].argmin()].me
        # noinspection PyTypeChecker
        if self.bonuses[0] and np.any(bonuses_distances[0, :] <= self.got_bonus_distance):
            self.bonuses[0] = False
            should_update = True
            if got_top_bonus_case:  # TODO обработка взятия бонуса другим игроком
                print "Tick %s, Got top bonus" % state.tick

        got_bottom_bonus_case = world.wizards[bonuses_distances[1, :].argmin()].me
        # noinspection PyTypeChecker
        if self.bonuses[1] and np.any(bonuses_distances[1, :] <= self.got_bonus_distance):
            self.bonuses[1] = False
            should_update = True
            if got_bottom_bonus_case:  # TODO обработка взятия бонуса другим игроком
                print "Tick %s, Got bottom bonus" % state.tick

        if self.distances[self.index_top_bonus] <= game.wizard_vision_range and self.bonuses[0] and all(((b.x, b.y) != BONUS_TOP for b in world.bonuses)):
            print state.tick, 'Top bonus not found'
            self.bonuses[0] = False
            should_update = True
        if self.distances[self.index_bottom_bonus] <= game.wizard_vision_range and self.bonuses[1] and all(((b.x, b.y) != BONUS_BOTTOM for b in world.bonuses)):
            print state.tick, 'Bottom bonus not found'
            self.bonuses[1] = False
            should_update = True

        self.tick = state.tick

        l1 = len(self.route)
        while len(self.route) > 1 and self.distances[self.current_hardpoint] <= self.get_passed_distance(self.current_hardpoint):
            self.prev_hardpoints.append(self.route.popleft())
            print self.tick, "Hardpoint reached: ", self.prev_hardpoints, self.route

        # go_to_enemies_base = False
        if not should_update and self.current_hardpoint != self.index_enemies_base and len(state.allies) > 0 and np.count_nonzero(
            cdist(
                state.positions[state.allies], self.hardpoints[self.index_enemies_base].reshape((1, 2))
            ) <= game.faction_base_radius + 500.0
        ) >= 4:
            # print state.tick, "Go to enemy base"
            should_update = True
            # self.route = deque([self.index_enemies_base])
            # go_to_enemies_base = True

        if not should_update and self.current_hardpoint != self.index_allies_base and len(state.enemies) > 0 and np.count_nonzero(
            cdist(
                state.positions[state.enemies], self.hardpoints[self.index_allies_base].reshape((1, 2))
            ) <= game.faction_base_radius + 700.0
        ) >= 3:
            # print state.tick, "Go to allies base"
            should_update = True
            # self.route = deque([self.index_allies_base])

        if not should_update and self.disabled:
            # print self.tick, "Update disabled router"
            should_update = True

        if should_update:
            # print self.tick, "Route calculation"
            self.weight = self.calc_weight(state, game)
            self.updated = self.tick

            if np.any(self.weight != 0.0):
                nhp = self.distances[self.active_hardpoints].argmin()
                route = deque(
                    self.restore_path(
                        self.active_hardpoints[nhp],
                        self.weight.argmax()
                    )
                )

                while len(route) > 1 and route[0] in self.prev_hardpoints:
                    route.popleft()

                if len(route) == len(self.route) and all((route[ii] == self.route[ii] for ii in xrange(len(route)))):
                    return False

                if sum(self.weight[route]) < 7.0 * sum(self.weight[self.route]):
                    return False

                print state.tick, "New route ", route, sum(self.weight[route]), self.route, sum(self.weight[self.route])
                self.route = route

                return True

        return l1 != len(self.route)
